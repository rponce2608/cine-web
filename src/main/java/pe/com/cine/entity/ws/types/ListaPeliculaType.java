
package pe.com.cine.entity.ws.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListaPeliculaType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListaPeliculaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pelicula" type="{http://cine.com.pe/entity/ws/types}PeliculaType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaPeliculaType", propOrder = {
    "pelicula"
})
public class ListaPeliculaType {

    protected List<PeliculaType> pelicula;

    /**
     * Gets the value of the pelicula property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pelicula property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPelicula().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PeliculaType }
     * 
     * 
     */
    public List<PeliculaType> getPelicula() {
        if (pelicula == null) {
            pelicula = new ArrayList<PeliculaType>();
        }
        return this.pelicula;
    }

}
