
package pe.com.cine.entity.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="listaGenero" type="{http://cine.com.pe/entity/ws/types}ListaGeneroType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listaGenero"
})
@XmlRootElement(name = "buscarResponse")
public class BuscarResponse {

    @XmlElement(required = true)
    protected ListaGeneroType listaGenero;

    /**
     * Obtiene el valor de la propiedad listaGenero.
     * 
     * @return
     *     possible object is
     *     {@link ListaGeneroType }
     *     
     */
    public ListaGeneroType getListaGenero() {
        return listaGenero;
    }

    /**
     * Define el valor de la propiedad listaGenero.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaGeneroType }
     *     
     */
    public void setListaGenero(ListaGeneroType value) {
        this.listaGenero = value;
    }

}
