package pe.com.cine.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pe.com.cine.entity.ws.types.BuscarPorGeneroRequest;
import pe.com.cine.entity.ws.types.BuscarPorGeneroResponse;
import pe.com.cine.entity.ws.types.BuscarRequest;
import pe.com.cine.entity.ws.types.BuscarResponse;
import pe.com.cine.entity.ws.types.GeneroType;
import pe.com.cine.entity.ws.types.ListaPeliculaType;
import pe.com.cine.entity.ws.types.PeliculaType;
import pe.com.cine.services.cinews.ws.CineWS;

@Controller
public class HomeController {
	// inject via application.properties

	@Value("${welcome.message}")
	private String message = "";

	@Value("${welcome.message.soap}")
	private String message_soap = "";

	@Value("${welcome.message.rest}")
	private String message_rest = "";

	@Autowired
	private CineWS cineWS;

	@RequestMapping("/")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);
		return "welcome";
	}

	@RequestMapping("/soap")
	public ModelAndView soap(@ModelAttribute GeneroType generoType) {
		ModelAndView mav = new ModelAndView("soap");
		Map<String, String> generoList = new HashMap<String, String>();

		BuscarRequest request = new BuscarRequest();

		BuscarResponse response = cineWS.buscarDatos(request);

		for (GeneroType genero : response.getListaGenero().getGenero()) {
			generoList.put(genero.getIdGenero(), genero.getNombre());
		}

		mav.addObject("message", this.message_soap);
		mav.addObject("generoType", generoType);
		mav.addObject("generoList", generoList);

		return mav;
	}

	@RequestMapping("/filter_movies")
	public ModelAndView all_movies(@ModelAttribute GeneroType generoType, @RequestParam("codigo") String codigo) {
		ModelAndView mav = new ModelAndView("soap");

		Map<String, String> generoList = new HashMap<String, String>();

		Map<String, PeliculaType> peliculaLista = new HashMap<>();

		BuscarRequest request = new BuscarRequest();

		BuscarPorGeneroRequest buscarPorGeneroRequest = new BuscarPorGeneroRequest();
		buscarPorGeneroRequest.setIdGenero(codigo);

		BuscarResponse response = cineWS.buscarDatos(request);

		BuscarPorGeneroResponse buscarPorGeneroResponse = cineWS.buscarDatosPeliculaPorGenero(buscarPorGeneroRequest);

		ListaPeliculaType lta = buscarPorGeneroResponse.getListaPelicula();

		for (GeneroType genero : response.getListaGenero().getGenero()) {
			generoList.put(genero.getIdGenero(), genero.getNombre());
		}

		if (lta != null) {
			for (PeliculaType peliculaType : lta.getPelicula()) {
				peliculaLista.put(peliculaType.getIdPelicula(), peliculaType);
			}
		}

		mav.addObject("message", this.message_soap);
		mav.addObject("generoType", generoType);
		mav.addObject("generoList", generoList);
		mav.addObject("peliculaList", peliculaLista);

		return mav;
	}

	@RequestMapping("/all_movies")
	public ModelAndView all_movies_v2(@ModelAttribute GeneroType generoType) {
		ModelAndView mav = new ModelAndView("soap");

		Map<String, String> generoList = new HashMap<String, String>();

		Map<String, PeliculaType> peliculaLista = new HashMap<>();

		BuscarRequest request = new BuscarRequest();

		BuscarPorGeneroRequest buscarPorGeneroRequest = new BuscarPorGeneroRequest();

		BuscarResponse response = cineWS.buscarDatos(request);

		BuscarPorGeneroResponse buscarPorGeneroResponse = cineWS.buscarDatosPeliculaPorGenero(buscarPorGeneroRequest);

		ListaPeliculaType lta = buscarPorGeneroResponse.getListaPelicula();

		for (GeneroType genero : response.getListaGenero().getGenero()) {
			generoList.put(genero.getIdGenero(), genero.getNombre());
		}

		if (lta != null) {
			for (PeliculaType peliculaType : lta.getPelicula()) {
				peliculaLista.put(peliculaType.getIdPelicula(), peliculaType);
			}
		}

		mav.addObject("message", this.message_soap);
		mav.addObject("generoType", generoType);
		mav.addObject("generoList", generoList);
		mav.addObject("peliculaList", peliculaLista);

		return mav;
	}

	@RequestMapping("/rest")
	public String rest(Map<String, Object> model) {
		model.put("message", this.message_rest);
		return "rest";
	}

}
