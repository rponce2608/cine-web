package pe.com.cine;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import pe.com.cine.services.cinews.ws.CineWS;

//@SpringBootApplication(scanBasePackages={"pe.com.cine","pe.com.cine.controller","pe.com.cine.entity.ws.types","pe.com.cine.services.cinews.ws"})
@SpringBootApplication
public class SpringBootWebApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootWebApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringBootWebApplication.class, args);
	}

	@Bean
	public JaxWsPortProxyFactoryBean proxyWSCine() throws MalformedURLException {
		JaxWsPortProxyFactoryBean cine = new JaxWsPortProxyFactoryBean();
		cine.setServiceInterface(CineWS.class);
		cine.setWsdlDocumentUrl(new URL("http://localhost:8091/ws/cine?wsdl"));
		cine.setNamespaceUri("http://cine.com.pe/services/cinews/ws");
		cine.setServiceName("cineWS");
		cine.setEndpointAddress("http://localhost:8091/ws/cine?wsdl");
		cine.setLookupServiceOnStartup(false);

		Map<String, Object> propiedades = new HashMap<String, Object>();
		propiedades.put("com.sun.xml.ws.request.timeout", 5000);
		propiedades.put("com.sun.xml.ws.connect.timeout", 5000);

		cine.setCustomProperties(propiedades);

		return cine;
	}
}
