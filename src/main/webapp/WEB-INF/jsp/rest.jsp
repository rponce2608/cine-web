<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

<!-- Access the bootstrap Css like this, 
		Spring boot will handle the resource mapping automcatically -->
<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<script type="text/javascript"
	src='<spring:url value="/js/jquery-1.9.1.min.js"/>'></script>

<script>
	$.ajax({
		type : 'GET',
		url : 'http://localhost:8090/genero/listar',
		dataType : 'json',
		success : function(result) {
			for (x = 0; x < result.length; x++) {
				$("#myselect").append(
						"<option value='" + result[x].idGenero + "'>"
								+ result[x].nombre + "</option>");
			}

		}
	});


	function cambio() {
		$('#records_table tbody > tr').remove();
		d = document.getElementById("myselect").value;
		$.ajax({
			type : 'GET',
			url : 'http://localhost:8090/pelicula/listarPorGenero/' + d,
			dataType : 'json',
			success : function(result) {
				var trHTML = '';
				$.each(result, function(i, item) {
					trHTML += '<tr><td>' + result[i].idPelicula + '</td><td>'
							+ result[i].genero.nombre + '</td><td>'
							+ result[i].titulo_local + '</td></tr>';
				});
				$('#records_table').append(trHTML);

			}
		});
	}

	function all_movies() {
		$("#myselect").val(0);
		$('#records_table tbody > tr').remove();
		$.ajax({
			type : 'GET',
			url : 'http://localhost:8090/pelicula/listar',
			dataType : 'json',
			success : function(result) {
				var trHTML = '';
				$.each(result, function(i, item) {
					trHTML += '<tr><td>' + result[i].idPelicula + '</td><td>'
							+ result[i].genero.nombre + '</td><td>'
							+ result[i].titulo_local + '</td></tr>';
				});
				$('#records_table').append(trHTML);

			}
		});
	}
</script>

<c:url value="/css/main.css" var="jstlCss" />

<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand">Cine</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/">Inicio</a></li>
					<li><a href="soap">WSDL</a></li>
					<li class="active"><a href="rest">REST</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">

		<div class="starter-template">
			<h1>Spring Boot Web JSP Example</h1>
			<h2>Message: ${message}</h2>
			<br/>
			<select id="myselect" onchange="cambio()">
			<option selected="selected" value="0">--- Seleccione una Pelicula ---</option>
			
			</select> 
			<button id="all" onclick="all_movies()">Ver todas las Peliculas</button>
			<br /> <br /> <br />

			<table id="records_table" border='1'>
				<thead>
					<tr>
						<th>Codigo Pelicula</th>
						<th>Codigo Genero</th>
						<th>Titulo Pelicula</th>
					</tr>
				</thead>
			</table>
		</div>

	</div>


	<script type="text/javascript"
		src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>