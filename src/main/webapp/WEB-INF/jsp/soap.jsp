<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html lang="en">
<head>

<!-- Access the bootstrap Css like this, 
		Spring boot will handle the resource mapping automcatically -->
<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<script type="text/javascript"
	src='<spring:url value="/js/jquery-1.9.1.min.js"/>'></script>

<c:url value="/css/main.css" var="jstlCss" />

<link href="${jstlCss}" rel="stylesheet" />

<script>
	var busqueda_all = null;
	function all_movies() {
		busqueda_all = "all";
	}

	function show_selected() {
		var selector = document.getElementById('myselect');
		var value = selector[selector.selectedIndex].value;
		if (busqueda_all == null) {
			if (value === 'NONE') {
				document.getElementById("myForm").action = "/soap";
			} else {
				document.getElementById("myForm").action = "/filter_movies?codigo="
						+ value;
			}
		} else {
			value = null;
			document.getElementById("myForm").action = "/all_movies";
		}

		document.getElementById("myForm").submit();
	}
</script>

</head>
<body>
	<form:form method="POST" commandName="generoType" 
		onsubmit="show_selected()" id="myForm">
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand">Cine</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/">Inicio</a></li>
						<li class="active"><a href="soap">WSDL</a></li>
						<li><a href="rest">REST</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<div class="container">

			<div class="starter-template">
				<h1>Spring Boot Web JSP Example</h1>
				<h2>Message: ${message}</h2>
				<br />
				<form:select path="nombre" id="myselect" onchange="show_selected()">
					<form:option value="NONE" label="--- Seleccione una Pelicula ---" />
					<form:options items="${generoList}" />
				</form:select>
				<button id="all" onclick="all_movies()">Ver todas las
					Peliculas</button>
				<br /> <br /> <br />

				<table id="records_table" border='1'>
					<thead>
						<tr>
							<th>Codigo Pelicula</th>
							<th>Codigo Genero</th>
							<th>Titulo Pelicula</th>
						</tr>
					</thead>

					<c:forEach var="movie" items="${peliculaList}" varStatus="status">
						<tr>
							<td>${movie.getValue().getIdPelicula()}</td>
							<td>${movie.getValue().getGenero().getNombre()}</td>
							<td>${movie.getValue().getTituloLocal()}</td>
						</tr>
					</c:forEach>
				</table>
			</div>

		</div>


		<script type="text/javascript"
			src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	</form:form>
</body>

</html>